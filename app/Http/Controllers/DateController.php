<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Date;
use App\Newsletter;

class DateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $this->authorize('before', Date::class);

         $emails = Newsletter::all();

        $dates = Date::where('dategig', '>', now())
                    ->orderBy('dategig', 'asc')
                    ->take(5)
                    ->get();


        return view('admin', compact('dates', 'emails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
       

        $validated = request()->validate([
            'dategig' => 'required',
            'location' => 'required',
            'venue' => 'required',
            'price' => 'required',
            'plus' => 'required'

        ]);

        Date::create($validated);

    

        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         
        $date = Date::findOrFail($id); // verfie si l'id existe

        return view('admin_edit', compact('date'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        

        $date = Date::findOrFail($id);

          $validated = request()->validate([
            'dategig' => 'required',
            'location' => 'required',
            'venue' => 'required',
            'price' => 'required',
            'plus' => 'required'

        ]);

          

        $date->update($validated);


        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
        Date::findOrFail($id)->delete();

        return redirect('/admin');
    }
}

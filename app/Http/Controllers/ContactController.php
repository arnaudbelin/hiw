<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    public function send(){

    	 $validated = request()->validate([
            'name' => 'required|min:2',
            'email' => 'required|email',
            'content' => 'required'
        ]);

    	

    	 Mail::to($validated['email'])->send(new ContactMail($validated));

    	 session()->flash('contact', 'Votre message a bien été envoyé, merci :)');



    	 return redirect('/#contact');



    }
}

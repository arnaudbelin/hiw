<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Date;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dates = Date::where('dategig', '>', now())
                    ->orderBy('dategig', 'asc')
                    ->take(5)
                    ->get();

        return view('hiw', compact('dates'));
    }
}

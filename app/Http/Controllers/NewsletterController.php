<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Newsletter;

class NewsletterController extends Controller
{
	
	

    public function store(){
    	$validated = request()->validate([
            'email' => 'required|email'
        ]);

        Newsletter::create($validated);

        session()->flash('newsletter', 'Merci, tu es inscrit à la newsletter de How It Works');

        return redirect('/#footer');
    }



    public function edit($id)
    {


         
        $email_newsletter = Newsletter::findOrFail($id); // verfie si l'id existe

        return view('admin_edit_email', compact('email_newsletter'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        

        $email_newsletter = Newsletter::findOrFail($id);

          $validated = request()->validate([
            'email' => 'required|email',
            

        ]);

          

        $email_newsletter->update($validated);


        return redirect('/admin/#newsletter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
        Newsletter::findOrFail($id)->delete();

        return redirect('/admin/#newsletter');
    }
}

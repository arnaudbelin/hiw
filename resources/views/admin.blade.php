@extends('layouts.app')

@section('content')

<div class="container col-6">
<form action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/admin" method="post">

	@method('POST') 
	@csrf

<h2>Ajouter un concert : </h2>
  <div class="form-group" >
    <label >Date</label>
    <input type="date" class=" form-control {{ $errors->has('dategig') ? 'form-control is-invalid' : ''}}" value="{{ old('dategig')}}"name="dategig">
     <div class="invalid-feedback">
    <p>{{ $errors->first('dategig') }}</p>
	</div>
  </div>
    

  <div class="form-group">
    <label >Ville</label>
    <input type="text" name="location"  class="form-control {{ $errors->has('location') ? 'form-control is-invalid' : ''}}" value="{{ old('location')}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('location') }}</p>
	</div>
  </div>

  <div class="form-group">
    <label >Salle</label>
    <input type="text" name="venue"  class="form-control {{ $errors->has('venue') ? 'form-control is-invalid' : ''}}" value="{{ old('venue')}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('venue') }}</p>
	</div>
  </div>

<div class="form-group">
    <label >Prix</label>
    <input type="text" name="price"  class="form-control {{ $errors->has('price') ? 'form-control is-invalid' : ''}}" value="{{ old('price')}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('price') }}</p>
	</div>
  </div>

  <div class="form-group">
    <label >Lien</label>
    <input type="text" name="plus"  class="form-control {{ $errors->has('plus') ? 'form-control is-invalid' : ''}}" value="{{ old('plus')}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('plus') }}</p>
	</div>
  </div>

   



    

  <button type="submit" class="btn btn-primary mt-3">Enregistrer</button>

  <br>
  <br>
  <br>

</form>

<div class="list_concert">

  <h2>Liste des concerts à venir : </h2>

	<table class="table">
    <thead>
        <tr>
            <th>Date</th>
            <th>Ville</th>
            <th>Salle</th>
            <th>Editer</th>
            <th>Suprimer</th>
        </tr>
    </thead>
    <tbody>
	@foreach($dates as $date)
			<tr>
    		<td>{{$date->dategig}}</td>
            <td>{{$date->location}}</td>
      		<td>{{$date->venue}}</td>
      		<td><a class="btn btn-primary" href="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/admin/{{$date->id}}/edit">Editer</a></td>
      		<td><form action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/admin/{{$date->id}}" method="POST">


		@method('DELETE') 
			@csrf	
	

	
	
	<button type="submit" class="btn btn-danger">Supprimer</button>
</form></td>
      		</tr>
    @endforeach
    </tbody>
</table>

	
</div>

<div class="newsletter" id="newsletter" >
  <br>
  <h2>Envoi de newsletter</h2>
  <br>

  <a class="btn btn-warning"href="mailto:@foreach($emails as $email){{$email->email}},@endforeach">Envoyer</a>
  <button id="show_contact" class="btn btn-warning"href="">Montrer les contacts</button>
  <br>
  <br>

  <div class="list_contact" id="list_contact" hidden>

  <h6>Liste des contacts : </h6>

  

  <table class="table">
    <thead>
        <tr>
            <th>Email</th>
            <th>Editer</th>
            <th>Suprimer</th>
            <th>Envoyer un email</th>
        </tr>
    </thead>
    <tbody>
  @foreach($emails as $email)
      <tr>
        <td>{{$email->email}}</td>
          <td><a class="btn btn-primary" href="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/newsletter/{{$email->id}}/edit">Editer</a></td>
          <td><form action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/newsletter/{{$email->id}}" method="POST">


    @method('DELETE') 
      @csrf 
  

  
  
  <button type="submit" class="btn btn-danger">Supprimer</button>
</form></td>
<td><a class="btn btn-info" href="mailto:{{$email->email}}">Envoyer un email</a></td>
          </tr>
    @endforeach
    </tbody>
</table>

  
</div>


</div>

</div>



@endsection

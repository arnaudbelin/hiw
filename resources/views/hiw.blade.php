<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
       <link rel="icon" href="http://arnaudbelin.sites.3wa.io/laraveltest/public/img/favicon.ico" />
        <script src="https://kit.fontawesome.com/4de7108ca3.js" crossorigin="anonymous"></script>
        <title>HOW IT WORKS &mdash; Take it away</title>
    </head>
    <body class="container">
        <header class="header">
            <div class="header__logo-box">
                <img src="img/logo-yellow.png" alt="Logo" class="header__logo">
            </div>
            <h1 class="header-1">HOW IT WORKS</h1>
            <h2 class="header-2"> - Take it away - </h2>
             

        </header>
       
        <section class="tour">
            <div class="tour__title-main">
                <h3 class="heading-3">&nbsp;Tour</h3>
            </div>
            <div class="tour__title tour__title-date heading-4">
                date
            </div>
            <div class="tour__title tour__title-location heading-4">
                lieu
            </div>
            <div class="tour__title tour__title-venue heading-4">
                salle
            </div>
           <!--  <div class="tour__title tour__title-price heading-4">
                prix
            </div> -->
            <div class="tour__title tour__title-plus heading-4">
                infos
            </div>

            @foreach($dates as $date)
            <div class="tour__content tour__content-date">
                {{$date->dategig}}
            </div>
            <div class="tour__content tour__content-location">
                {{$date->location}}
            </div>
            <div class="tour__content tour__content-venue">
                 {{$date->venue}}
            </div>
           <!--  <div class="tour__content tour__content-price">
                 {{$date->price}}
            </div> -->
            <div class="tour__content tour__content-plus">
                <a class="button button-plus" target="_blank"  href="{{$date->plus}}"><span class="btn-hide">En savoir </span><i class="fas fa-plus-square"></i></a>
            </div>

            @endforeach
        </section>
        <section class="music">
            <div class="music__title">
                <h3 class="heading-3">&nbsp;Music</h3>
            </div>
            <div class="card">
                <div class="card__side card__side--front card__side--front-1">
                    <i class="fas fa-compact-disc"></i>
                </div>
                <div class="card__side card__side--back card__side--back-1">
                    <div class="card__picture card__picture--1">
                        &nbsp;
                    </div>
                    <h4 class="card__heading card__heading--1">
                    <span class="card__heading-span card__heading-span--1">Take it away</span></h4>
                    <div class="card__details card__details--1">
                        <ul>
                            <li>01. BULLSHIT DISCHARGE</li>
                            <li>02. I WANT</li>
                            <li>03. MY CHOICE MY WAY</li>
                            <li>04. PARASITE</li>
                            <li>05. RIGHT NOW</li>
                            <li>06. TAKE IT AWAY</li>
                            <li>07. TO THE MEMORY OF DOOM</li>
                        </ul>
                    </div>
                    <center><a  target="_blank" href="https://howitworkshxc.bandcamp.com/releases" class="button button-buy">Acheter l'album</a></center>
                </div>
            </div>
            <div class="card">
             <img src="img/logo-mediumblue.png" alt="Logo" class="music__logo">
        </div>
            <div class="card">
                <div class="card__side card__side--front card__side--front-2">
                    <i class="fas fa-play-circle"></i>
                </div>
                <div class="card__side card__side--back card__side--back-2">
                    <iframe style="border: 0; width: 350px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=3997933771/size=large/bgcol=333333/linkcol=ffffff/transparent=true/" seamless><a href="http://howitworkshxc.bandcamp.com/album/take-it-away">TAKE IT AWAY by HOW IT WORKS</a></iframe>
                </div>
            </div>
            <div class="bandcamp_bg">
                &nbsp;
            </div>
            <div class="bandcamp_tab">
                  <iframe style="border: 0; width: 375px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=3997933771/size=large/bgcol=333333/linkcol=ffffff/transparent=true/" seamless><a href="http://howitworkshxc.bandcamp.com/album/take-it-away">TAKE IT AWAY by HOW IT WORKS</a></iframe>
                </div>
                 <div class="bandcamp_phone">
                 <iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=3997933771/size=large/bgcol=333333/linkcol=ffffff/tracklist=false/artwork=none/transparent=true/" seamless><a href="http://howitworkshxc.bandcamp.com/album/take-it-away">TAKE IT AWAY by HOW IT WORKS</a></iframe>
        </section>
        <section class="video">
            <div class="video__title">
                <h3 class="heading-3">&nbsp;Video</h3>
            </div>
            <video class="video__clip" controls width="900" ><source src="img/hiwlowq.mp4" type="video/mp4"></video>
            <div class="video__bottom">
                <!-- <img src="img/crossyellow.png" alt="Logo" class="video__bottom-logo"> -->
            </div>
        </section>
        <section class="social">
            <div class="social__title">
                <h3 class="heading-3">&nbsp;Social feeds</h3>
            </div>
            <div class="social__facebook">
                <div id="fb-root"></div>
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v6.0"></script>
                <div class="fb-page" data-tabs="timeline,events,messages" data-href="https://www.facebook.com/howitworkshxc/" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"></div>
            </div>
            <div class="social__youtube">
                <iframe class="social__youtube-1" width="800" height="444" src="https://www.youtube.com/embed/GlYhjeJ-g-k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </section>
       <!--  <section class="contact" id="contact">
            <div class="contact__title">
                <h3 class="heading-3">&nbsp;Contact</h3>
            </div>

@if(session()->has('contact'))
			
			<div class="contact__flash">

  			{{session()->get('contact')}}

			</div>

			@endif
           


            <form class="contact__form" action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/contact" method="post">
            	@method('POST') 
				@csrf
				
                <div class="contact__form-group-name">
                    <input name ="name" class="contact__form__input contact__form__input-name" type="text" id="name" placeholder="Ton nom" required autocomplete="off" value="{{ old('name')}}">
                    <label for="name" class="contact__form__label contact__form__label-name">Ton Nom</label>
                </div>
                <div class="contact__form-group-email">
                    <input name="email" class=" contact__form__input contact__form__input-email" value="{{ old('email')}}" type="email" id="email" placeholder="Ton email" required autocomplete="off">
                    <label for="email" class="contact__form__label contact__form__label-email">Ton email</label>
                </div>
                <div class="contact__form-group-message">
                    <textarea name="content" class="contact__form__input contact__form__input-message" type="textarea" value="{{ old('content')}}" id="message" placeholder="Ton message..." rows="10" required></textarea> 
                    <label for="message" class=" contact__form__label contact__form__label-message">Ton message...</label>
                </div>
                <div class="contact__form-group-btn">
                    <button type="submit" class="button button-send">ENVOYER</button>
                <div class="contact__form-group-btn">
            </form>
        </section> -->
        <section class="gallery">
            <figure class="gallery__item gallery__item--1"></figure>
            <figure class="gallery__item gallery__item--2"></figure>
            <figure class="gallery__item gallery__item--3"></figure>
            <figure class="gallery__item gallery__item--4"></figure>
            <figure class="gallery__item gallery__item--5"></figure>
            <figure class="gallery__item gallery__item--6"></figure>
            <figure class="gallery__item gallery__item--7">
                <img src="img/logo-yellow.png" alt="Logo" width="100%">
            </figure>
            <figure class="gallery__item gallery__item--8"></figure>
            <figure class="gallery__item gallery__item--9"></figure>
            <figure class="gallery__item gallery__item--10"></figure>
            <figure class="gallery__item gallery__item--11"></figure>
            <figure class="gallery__item gallery__item--12"></figure>
            <figure class="gallery__item gallery__item--13"></figure>
            <figure class="gallery__item gallery__item--14"></figure>
        </section>
        <section class="footer" id="footer">
            <div class="footer__logo">
                <img src="img/logo-font-title.png" alt="Logo" class="Logo__hiw" width="100px">
                <hr class="footer__logo-hr">
            </div>
            <div class="footer__link">
                <a target="_blank"  href ="https://fr-fr.facebook.com/howitworkshxc/"><i class="fab fa-facebook"></i></i></a>
                <a target="_blank" href ="https://howitworkshxc.bandcamp.com/album/take-it-away"><i class="fab fa-bandcamp"></i></a>
                <a target="_blank" href ="https://www.leprozy.com/GAM%20website/pagefragroupekingdom.htm"><img src="img/logo-gam-yellow.png" alt="Logo_GAM" class="Logo__gam" width="50px"></a>
            </div>
            <div class="footer__newsletter">
            	@if(session()->has('newsletter'))
			
			<div class="contact__flash">

  			{{session()->get('newsletter')}}

			</div>

			@endif
                <form class="footer__form" action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/newsletter" method="post">
                	@method('POST') 
					@csrf	
                    <input class="footer__newsletter-input" type="email" id="email" placeholder=" Ton email" required autocomplete="off" name="email">
                    <button type="submit" href="#" class="button button-news">S'inscrire à la newsletter</button>
                </form>
            </div>
            <div class="footer__copyright">
                &copy; site powered by Powerwolf-design. For any questions, you can contact us by email <a class="footer__copyright-a" href="mailto:belinarnd@gmail.com">here.</a>
            </div>
        </section>
    </body>
</html>

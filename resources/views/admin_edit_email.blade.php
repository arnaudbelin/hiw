@extends('layouts.app')

@section('content')

<div class="container col-6">


  <form action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/newsletter/{{$email_newsletter->id}}" method="POST">

  @method('PATCH') 
  @csrf

<h2>Editer un mail de la newsletter</h2>
  <div class="form-group" >
    <label >Email : </label>
    <input type="email" class=" form-control {{ $errors->has('email_newsletter') ? 'form-control is-invalid' : ''}}" value="{{$email_newsletter->email}}"name="email">
     <div class="invalid-feedback">
    <p>{{ $errors->first('email_newsletter') }}</p>
	</div>
  </div>
        

  <button type="submit" class="btn btn-primary mt-3">Editer</button>

</form>





@endsection

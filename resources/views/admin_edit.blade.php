@extends('layouts.app')

@section('content')

<div class="container col-6">


  <form action="https://arnaudbelin.sites.3wa.io/laraveltest/public/index.php/admin/{{$date->id}}" method="POST">

  @method('PATCH') 
  @csrf

<h2>Ajout d'une date de concert</h2>
  <div class="form-group" >
    <label >Date</label>
    <input type="date" class=" form-control {{ $errors->has('dategig') ? 'form-control is-invalid' : ''}}" value="{{$date->dategig}}"name="dategig">
     <div class="invalid-feedback">
    <p>{{ $errors->first('dategig') }}</p>
	</div>
  </div>
    

  <div class="form-group">
    <label >Ville</label>
    <input type="text" name="location"  class="form-control {{ $errors->has('location') ? 'form-control is-invalid' : ''}}" value="{{$date->location}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('location') }}</p>
	</div>
  </div>

  <div class="form-group">
    <label >Salle</label>
    <input type="text" name="venue"  class="form-control {{ $errors->has('venue') ? 'form-control is-invalid' : ''}}" value="{{$date->venue}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('venue') }}</p>
	</div>
  </div>

<div class="form-group">
    <label >Prix</label>
    <input type="text" name="price"  class="form-control {{ $errors->has('price') ? 'form-control is-invalid' : ''}}" value="{{$date->price}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('price') }}</p>
	</div>
  </div>

  <div class="form-group">
    <label >Lien</label>
    <input type="text" name="plus"  class="form-control {{ $errors->has('plus') ? 'form-control is-invalid' : ''}}" value="{{$date->plus}}" rows="9">
     <div class="invalid-feedback">
    <p>{{ $errors->first('plus') }}</p>
	</div>
  </div>

   



    

  <button type="submit" class="btn btn-primary mt-3">Editer</button>

</form>





@endsection

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('hiw');
// });

Route::post('/contact', 'ContactController@send');

Route::resource('/admin', 'DateController')->middleware('auth'); 

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');




Route::resource('/newsletter', 'NewsletterController');